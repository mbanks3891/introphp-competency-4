<!--display a user regisration form using fields from(COMMON WITH?) the CUSTOMER table

Email address - required, valid email, maxlength 60
First Name - required, maxlength 40
Last Name - required, maxlength 20
Password - required, 8-20 characters
Confirm Password - required, 8-20 characters, must match Password
(refer to comp lab comp 2.5)


read the records *from the genre table* and display each one as a checkbox using the
GENREID as the value
and the 
NAME as the label
(see lab 3-5 where we used dropdown from employee table)

instruct the user to select up to 3 favorite genres..  user must select at least 1 genre and no more than 3

add a submit button with value of 'register'

when the form is submitted, validate the information and display error messages if there are any errors




IF the data fromthe for is valid.. create cookies:


name: first and last names from the form with a space in between, expires: 30 days from current date

genre1: the first genre selected by the user, expires: 1 year from current date

genre2: the second genre selected by the user, expires: 1 year from current date. 
If no selection was made, do not create cookie.

genre3: the third genre selected by the user, expires: 1 year from current date. 
If no selection was made, do not create cookie.

coupon: value = .25, temporary cookie. 

---------------
4-26-19-fri-1442 still need
correct form validation, done1506

correct cookie expiration, done1506


-->


<?php

require_once "comp4functions.php";
writeHead("REGISTRATION","Competency 4, Part A- Read & Write Cookies");

if(isset($_POST['register']))///////if 'register' clicked... gather data from form and validate
{$valid=true;


//required, valid email, maxlength 60
$email = htmlspecialchars($_POST['email']);
if(empty($email))
{echo"<p class='error'>Please enter your email!!!</p>";//required
$valid=false;}

if (!preg_match('/[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}/',$email))
{echo "<p class='error'>(Invalid email address)</p>";
$valid=false;}


if (strlen($email) > 60)
{echo"<p class='error'>email too long, max 60 characters!!!</p>";//max length 60
$valid=false;}





$firstname = htmlspecialchars(trim($_POST['firstname']));
if (empty($firstname))
{echo"<p class='error'>Please enter your first name!!!</p>";//required
$valid=false;}
if (strlen($firstname) > 40)
{echo"<p class='error'>first name too long, max 40 characters!!!</p>";//max length 40
$valid=false;}


$lastname = htmlspecialchars(trim($_POST['lastname']));
if(empty($lastname))
{echo"<p class='error'>Please enter your last name!!!</p>";//required
$valid=false;}
if (strlen($lastname) > 20)
{echo"<p class='error'>last name too long, max 20 characters!!!</p>";//max length 20
$valid=false;}


//required, 8-20 characters
$password = htmlspecialchars($_POST['password']);
if(empty($password))
{echo"<p class='error'>Please enter your password!!! </p>";
$valid=false;}
if (!preg_match('/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{8,20}$/',$password)) 
{echo "<p class='error'>(Password must have 1 uppercase letter, 1 lowercase letter, 1 number and be 8-20 characters in length)</p>";}

$pwconf=htmlspecialchars(trim($_POST['pwconf']));
if (empty($pwconf)){echo"<p class='error'>Please confirm the password</p>"; $valid=false;}
if (strcmp($password,$pwconf)!=0){echo "<p class='error'>Passwords do not match</p>"; $valid=false;}



//if something in the genre boxes is selected
if (isset($_POST['genrearray'])){
$genre=$_POST['genrearray'];

//if more than 3 items added to array
if (count(($_POST['genrearray'])) > 3)
{
$valid=false;
echo "<p class='error'><strong>Only 3 genres allowed... You chose too many: </strong><br>";
//need to read out genrearray contents...
foreach($_POST['genrearray'] as $arraykey=>$arrayvalue)
{echo " | ".$arrayvalue;}

	if (count(($_POST['genrearray'])) == 4) 
	{echo "<br><em><strong>Please omit one of your selections...</strong></em>";}
	else
	{echo "<br><em><strong>Please omit some of your selections...</strong></em>";}
}
}
//if nothing added to genre array
else 
{
echo "<p class='error'>Please select at least one genre</p>";
$valid=false;
$genre[0]="";
}


////////////////////if 'register' was clicked + if form was validated
if($valid)

//create cookies with some of the form data......
{
$fullname=$firstname . " " . $lastname;
setcookie('name',$fullname,time()+(60*60*30));//name cookie valid 30 days


$namesuffix=1;
while ($namesuffix < 3){//only create as many cookies as there were items selected.. per validation there should be three
foreach($_POST['genrearray'] as $arraykey=>$arrayvalue)
{$cookiename=$arrayvalue;//.$namecounter;
//$namecounter=$namecounter+1;

setcookie('genre'.$namesuffix,$cookiename,time()+(60*60*365));//genre cookie(s) valid 1 year
//setcookie('genre'.$namecounter,$cookiename,time()+(60*60*48));
//setcookie('genre'.$namecounter,$cookiename,time()+(60*60*48));

$namesuffix++;
}
}

setcookie('coupon',.25);//coupon cookie no expiration



//////////////////////////////////////after validated and cookies created, send to index page
header("Location:comp4assmtAindex.php");
exit();
}


}
// if the form register button was not clicked aka first visit to page... 
//nothing above runs..
//start here instead
//initialize php vars to blank and proceed to show the form with blank fields
else{
$email="";
$firstname="";
$lastname="";
$password="";
$genre[0]="";	
}

?>

<form method='post' action='comp4assmtAregister.php'>
<p>
<label for="email">E-Mail</label>
<input type="text" name="email" id="email" value="<?php echo $email;?>">
</p>
<p>
<label for="firstname">First Name</label>
<input type="text" name="firstname" id="firstname" value="<?php echo $firstname;?>">
</p>
<p>
<label for="lastname">Last Name</label>
<input type="text" name="lastname" id="lastname" value="<?php echo $lastname;?>">
</p>
<p>
<label for="password">Password</label>
<input type="password" name="password" id="password" value="<?php echo $password;?>">
</p>
<p>
<label for="confpassword">Confirm Password:</label>
<input type="password" name="pwconf" id="pwconf">
</p>

<!--///////CHECKBOXES///////////////////////////////////////////////////-->
<!--reads from the genre table and display each record as checkboxes on form-->
<!--there are 25 records.. refer to sampler file re: how to read and output....-->

<p>
Genre: <em>(Select between 1 and 3 genres)</em>
<br>
<?php
$conn =  createConn();
$gid="";
$gname="";
$linecounter=0;
$query="select GenreId, Name from Genre";
$result=mysqli_query($conn,$query);

if(!$result){die(mysqli_error($conn));}
//if no results, error out

if(mysqli_num_rows($result) > 0){

while ($row = mysqli_fetch_assoc($result)){
$gid=$row['GenreId'];
$gname=$row['Name'];

echo "<input type = 'checkbox' name='genrearray[]' id='$gid' value='$gid'"; 
//*ISSUE 4-25-19-thurs-1812.. value should be genre id not genre name.... only label should be genre name
//changed value from '$gid' to '$genre' 4-25-19-thurs-1500= SUCCESS

//for each element in genre array.. as '$genrearrayitem'.. 
//if it was submitted as checked in the form...
//it gets the word 'checked' in its checkbox code, after the value..  
//so it will retain its checkmark after form is rerun with error messages that prevented submission

foreach($genre as $genrearrayitem)
//$genre represents what was pulled from form as CHECKED..an array position aka a NUMBER
{
if($genrearrayitem == $gid)
//4-25-19-1824 using $gid, label check is working, but nothing stays checked after refresh
//changed '== $gid' to '== $gname' bc checkmarks arent holding after error 4-25-19-thurs-1509= SUCCESS
//4-25-19-1521 NVM, labels only checking AFTER first submission.........
//using $gid means no checks retained after submission + no label clikablity
//using $gname gives retained checks, but no label clickability.. except those taht were already checked, after first run
{echo " checked ";}
}

//changed label for=gid to label for=gname 4-25-19-thurs-1500= SUCCESS
//label can also be gid... works both ways.......
echo "><label for=$gid>$gname</label>";//finish checkbox form code.. apply label
//try swapping 'for=$gid' to 'for=$gname'..4-25-19-1527
//working both ways with no differences......
$linecounter=$linecounter + 1;

if($linecounter>4){
echo"<br>";
//only print 4 checkboxes/labels per line
$linecounter=0;}

}//end while results
}//end if results
?>



<p>
<input type="submit" name="register" value="Register">
</p>
</form>


<?php
writeFoot("4A");
?>