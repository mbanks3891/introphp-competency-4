<?php
ob_start();
require_once 'comp4functions.php';
writeHead("ERROR", "Comp 4.4- User Authentication");
session_start();

?>

<h3 align="center"><u>ACCOUNT LOCKED</u></h3>
<div align="center"><em>Please contact tech support.</em></div>

<?php writeFoot("4C"); ?>