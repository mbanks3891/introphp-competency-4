
<!--for index page:
add header and footer functions for html
after header, 
check to see if the 'name' cookie exists... if so, display 'welcome, name!'...


check to see if the 'coupon' cookie exists...  if so, display 'order today to receive 25pct off order!'...

check to see if the 'genre1' cookie exists...  if so, get the value
and check for the 'genre2' and 'genre3' cookies and get those values if they exist

create a query to select a max of 20 tracks from the TRACKS table that have a genre that matches one of the genres entered by the user.
(i.e. Select * from tracks where genreId=1 or genreId=2 or genreId=3 limit 20)

if no genre1 cookie exists, just create a simple query to select the first 20 tracks
(i.e. Select * from tracks limit 20)

Execute the SQL Query


On the page, display a heading 'Tracks you may like'...


List the Name, GenreId, Composer, and UnitPrice for the 20 tracks in your data set.  If the currency is not US dollars.. multiply by the conversion factor from the array and display in the preferred currency


Include a LINK to the register.php page


test to make sure the index page displays everything properly with and without cookies


---------------
4-26-19-fri-1442 still need
currency conversion

-->



<?php

require_once "comp4functions.php";
writeHead("INDEX","Competency 4, Part A- Read & Write Cookies");
$conn =  createConn();
/////////////////////////////////////////////////////////////////////////setup functions/html/db connection


//////////////////////////////////////////////////PROCESS COOKIES
//if name cookie set, say welcome
if(isset($_COOKIE['name'])){echo "Welcome, " . $_COOKIE['name'] . "!";}

//if discount cookie set, say discount
if(isset($_COOKIE['coupon'])){echo " Order today to receive 25% off your entire order!";}

//if genre1 cookie set, fill $genre1 variable 
//+ check for genre2 and genre3 cookies and fill vars if needed
//+ create query if genre cookies found
if(isset($_COOKIE['genre1']))
{
	$genre1=$_COOKIE['genre1'];
	$query = "select * from Track where GenreId = $genre1 Limit 20";

	if(isset($_COOKIE['genre2']))
		{$genre2=$_COOKIE['genre2'];
		$query = "select * from Track where GenreId = $genre2 or GenreId = $genre1 Limit 20";}

	if(isset($_COOKIE['genre3']))
		{$genre3=$_COOKIE['genre3'];
		$query = "select * from Track where GenreId = $genre1 or GenreId = $genre3 or GenreId = $genre2 Limit 20";}
}
else{
//if no genre cookie exists....
//NOTE to be here at all, you would have a NAME and at least ONE GENRE selected (unless direct via url)
$query = "select * from Track Limit 20";
};
?>

<u><h2>Tracks You May Like</h2></u>
<table>
<tr><th>Name</th><th>GenreID</th><th>Composer</th><th>UnitPrice</th></tr>

<?php

$result = mysqli_query($conn,$query);
// check for errors
if (!$result) {
die(mysqli_error($conn));
}
// check for results
if (mysqli_num_rows($result)> 0) {
// loop through results and display
while ($row = mysqli_fetch_assoc($result)) {
echo "<tr><td>".$row['Name']."</td>";
echo "<td>".$row['GenreId']."</td>";
echo "<td>".$row['Composer']."</td>";
echo "<td>".$row['UnitPrice']."</td></tr>";
}
} 
?>

</table>
<p><a href="comp4assmtAregister.php">Registration Page</a>


<?php
writeFoot("4A");
?>