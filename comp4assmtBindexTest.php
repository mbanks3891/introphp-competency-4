
<?php

ob_start();


require_once "comp4functions.php";
writeHead("INDEX","Competency 4, Part B Sessions");
$conn =  createConn();


session_start();


//before running querys and table output, check for isset ADD or isset DELETE

if (isset($_GET['add']))
{
$tracknum=$_GET['hiddenformid'];//reference form field for track id you want to add

	if (isset($_SESSION['playlistcount']))
	{
	$_SESSION['playlistcount']++;//if SESSIONVAR[playlistcount] exists, increment by 1
	$count = $_SESSION['playlistcount']; //set $count var for output loop limit later
	}
	else{
	$_SESSION['playlistcount']=1; //if SESSIONVAR[playlistcount] did not exist, it is created now with value of 1
	$count=	$_SESSION['playlistcount']; //set $count var for output loop limit later
	}
$_SESSION['playlistarray'][$count]=$tracknum; //contents of latest spot in array gets the trackid you're adding



//DATA FOR TESTING:
echo "Count@Add=$count<br>";
echo "PlaylistCountArray@Add=".$_SESSION['playlistcount']."<br><br>";
}



if (isset($_GET['delete']))
{

$trackidfromform=$_GET['trackidtodelete'];//reference form field for track id you want to delete
$linetodelete=$_GET['lineidtodelete'];//reference form field for line you want to delete
$count=$_SESSION['playlistcount'];//set $count var for loop limit 


	for ($i=1; $i<=$count; $i++)//loop iterations = amount of $count
	{//start at element 1 of playlistarray aka first item in list of added songs
		if (isset($_SESSION['playlistarray'][$i]))//if the element exists aka dont get stuck on empty/previously deleted elements...
		{
		
		//if ($idselected = ($_SESSION['playlistarray'][$d]))
		//{ // assign elements values, starting at element1,  to 'idselected' variable

			//for ($x=1; $x<$count; $x++)
			//{
			if  ($linetodelete == $i)//if line# from deleted item matches place in this sequence (aka do not delete all instances of track id)
			{
			$idselected = $_SESSION['playlistarray'][$i];
			
				if ($idselected == $trackidfromform)//AND if element's contents match trackid from form's delete button..then unset array element
				{

				$deletedindex=$i;
				unset($_SESSION['playlistarray'][$i]);
				$_SESSION['playlistcount'] = $_SESSION['playlistcount']-1;
				$count=$_SESSION['playlistcount'];
				//delete contents of that array element
				
				
					for ($x=$deletedindex; $x<=$count; $x++)
					{
					$_SESSION['playlistarray'][$x] = $_SESSION['playlistarray'][$x+1]; 
					}
				//$_SESSION['playlistarray'][$i] = $_SESSION['playlistarray'][$i+1]; 
				//move each subsequent array element's content's one element DOWN
				
				
					

				}
			}	
			//}//end for2
		//after deleting an array item, move each subsequent array item down one space
	
		//}//end if array element matches
		}//end if isset array element	
	
	}//end for1 
	
	//$d--;
	

$afterdeletecount=$_SESSION['playlistcount'];

/*
for ($i=$deletedindex; $i<$afterdeletecount; $i++)
{
	if (isset($_SESSION['playlistarray'][$i]))
	{
	$_SESSION['playlistarray'][$i] = $_SESSION['playlistarray'][$i+1];
	}
}
*/

//DATA FOR TESTING:
echo "id selected from array=".$idselected."<br>";
echo "id sent from form=".$trackidfromform."<br>";
echo "line to delete=".$linetodelete."<br>";
echo "Count@Delete=".$count."<br>";
echo "PlaylistCountArray@Delete=".$_SESSION['playlistcount']."<br><br>";

	//if ($_SESSION['playlistcount'] == 0)
	if ($count<=0)	
	{
	//DATA FOR TESTING:
	echo "Count@DeleteALL=$afterdeletecount<br>";
	echo "PlaylistCountArray@DeleteALL=".$_SESSION['playlistcount']."<br><br>";

	unset($_SESSION);
	session_destroy();
	}	

}// end if isset delete




//SESSION[playlistarray] is set based on what has been added/deleted..

//should run if 'add' has been clicked *at some point*, AKA SESSION[playlistcountarray] was created
if (isset($_SESSION['playlistcount']))
{

//$count=$_SESSION['playlistcount'];//set $count var for output loop limit 
//DATA FOR TESTING:
echo "Count@Output=$count<br>";
echo "PlaylistCountArray@Output=".$_SESSION['playlistcount']."<br><br>";

?><u><h2>Your Playlist</h2></u><!--only want to see this heading if SESSION[playlistarray] exists-->
<table>
<tr><th>TrackName</th><th>Composer</th><th>Genre</th><th></th></tr><?php

	for ($i=1; $i<=$count; $i++)
	{
	
	//query for and then print ALL array positions starting at 1, EACH time the page loads/'add' is clicked
	//only the last one appears to print.. but they are all actually reprinting each time page loads/this loop runs
	//note array position 0 is not used....

	
		if (isset($_SESSION['playlistarray'][$i]))//if the element exists
		{
		$line=$i;//dont increment line if array element is empty aka element contents were unset/deleted
		$chosenid = ($_SESSION['playlistarray'][$i]);//chosentrackid = trackid from the array element $i
		$query = "Select Name, Composer, GenreId from Track where TrackId = $chosenid"; 
		$result = mysqli_query($conn,$query); if (!$result) {die(mysqli_error($conn));}
			
			if (mysqli_num_rows($result)> 0) 
			{
			$row = mysqli_fetch_assoc($result);

			echo "<tr><td>".$row['Name']."</td>";
			echo "<td>".$row['Composer']."</td>";
			echo "<td>".$row['GenreId']."</td>";

			?><td><form action="comp4assmtBindex.php" method="get">
			<!--<input type='hidden' name='idfordelete' value="<?php echo $chosenid; ?>">-->
			<label for="line">trackid#:</label><input type='text' name='trackidtodelete' size="3" value="<?php echo $_SESSION['playlistarray'][$i]; ?>">
			<label for="line">Line:</label><input type='text' name='lineidtodelete' size="3" value="<?php echo $line; ?>">
			<input type="submit" name="delete" value="remove"></form></td></tr><?php
			}//end if sqli result
		
		}//end if isset array
	
	}//end forloop

}//end if isset playlist count

echo "</table>";
?>




<?php
//////////////////////////////////////////////////PROCESS COOKIES
//if name cookie set, say welcome
if(isset($_COOKIE['name'])){echo "Welcome, " . $_COOKIE['name'] . "!";}

//if discount cookie set, say discount
if(isset($_COOKIE['coupon'])){echo " Order today to receive 25% off your entire order!";}

//if genre1 cookie set, fill $genre1 variable 
//+ check for genre2 and genre3 cookies and fill vars if needed
//+ create query if genre cookies found
if(isset($_COOKIE['genre1']))
{
	$genre1=$_COOKIE['genre1'];
	$query = "select * from Track where GenreId = $genre1 Limit 20";

	if(isset($_COOKIE['genre2']))
		{$genre2=$_COOKIE['genre2'];
		$query = "select * from Track where GenreId = $genre2 or GenreId = $genre1 Limit 20";}

	if(isset($_COOKIE['genre3']))
		{$genre3=$_COOKIE['genre3'];
		$query = "select * from Track where GenreId = $genre1 or GenreId = $genre3 or GenreId = $genre2 Limit 20";}
}
else{
//if no genre cookie exists....
//NOTE to be here at all, you would have a NAME and at least ONE GENRE selected (unless direct via url)
$query = "select * from Track Limit 20";
};
?>

<u><h2>Tracks You May Like</h2></u>
<table>
<tr><th>Name</th><th>GenreID</th><th>Composer</th><th>UnitPrice</th><th></th></tr>

<?php

$result = mysqli_query($conn,$query);
// check for errors
if (!$result) {
die(mysqli_error($conn));
}
// check for results
if (mysqli_num_rows($result)> 0) {
// loop through results and display
while ($row = mysqli_fetch_assoc($result)) {
echo "<tr><td>".$row['Name']."</td>";
echo "<td>".$row['GenreId']."</td>";
echo "<td>".$row['Composer']."</td>";
echo "<td>".$row['UnitPrice']."</td>";

?>

<td>
<!---should be POST not 'gets' or even 'get'--->
<!--needs to be POST because POST['remember'] is not set.....-->
<form method='get' action='comp4assmtBindex.php'>
<!--should call script or cart????-->
<input type='hidden' name='hiddenformid' value="<?php echo $row['TrackId']; ?>">
<input type='submit' name='add' value='Add to playlist'>
</form></td></tr>

<?php
}
} 
?>

</table>
<p><a href="comp4assmtBregister.php">Registration Page</a>


<?php
writeFoot("4B");
?>