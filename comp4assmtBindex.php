
<?php

ob_start();


require_once "comp4functions.php";
writeHead("INDEX","Competency 4, Part B- Sessions");
$conn =  createConn();


session_start();


//before running queries and table output, check for isset ADD and isset DELETE

if (isset($_GET['add']))
{
$tracknum=$_GET['hiddenformid'];//reference form field for track id you want to add

	if (isset($_SESSION['playlistcount']))
	{
	$_SESSION['playlistcount']++;//if SESSIONVAR[playlistcount] exists, increment by 1
	$count = $_SESSION['playlistcount']; //set $count var for output loop limit later
	}
	else{
	$_SESSION['playlistcount']=1; //if SESSIONVAR[playlistcount] did not exist, it is created now with value of 1
	$count=	$_SESSION['playlistcount']; //set $count var for output loop limit later
	}
$_SESSION['playlistarray'][$count]=$tracknum; //contents of latest spot in array gets the trackid you're adding
}



if (isset($_GET['delete']))
{

$trackidfromform=$_GET['trackidtodelete'];//reference form field for track id you want to delete
$linetodelete=$_GET['lineidtodelete'];//reference form field for line you want to delete
$count=$_SESSION['playlistcount'];//set $count var for loop limit- based on how many were added


	for ($i=1; $i<=$count; $i++)//loop iterations = amount of $count
	{//start at element 1 of playlistarray aka first item in list of added songs (skipped element 0)
		
		if (isset($_SESSION['playlistarray'][$i]))
		{//if the element exists aka dont get stuck on empty/previously deleted elements...
			
			if  ($linetodelete == $i)
			{//if line# from deleted button matches $i counter (aka do not delete all instances of track id)
			
			$idselected = $_SESSION['playlistarray'][$i];
			//capture the array element from that line
			
				if ($idselected == $trackidfromform)
				{//verify the array element from the line matches the track id you want to delete
				
				$deletedindex=$i;
				unset($_SESSION['playlistarray'][$i]);
				//erase array element's contents
				$_SESSION['playlistcount'] = $_SESSION['playlistcount']-1;
				//decrement playlist count
				$count=$_SESSION['playlistcount'];
				
				
					for ($x=$deletedindex; $x<=$count; $x++)
					{
					$_SESSION['playlistarray'][$x] = $_SESSION['playlistarray'][$x+1]; 					
					//after deleting an array item's contents, move each subsequent array item's contents down by one space
					//do not move anything in the preceeding array elements.... only start with blank element +1

					}//endfor elements between deleted item and total of count
					
				}//endif id matches button

			}//endif line matches counter

		}//endif isset array element	

	}//endfor loop iterations = amount of $count
	

	if ($count<=0)	
	{//don't print playlist header if session is unset 
	unset($_SESSION);
	session_destroy();
	}
		

}// end if isset delete






//will only run if 'add' has been clicked *at some point*, AKA SESSION[playlistcount] was initialized
if (isset($_SESSION['playlistcount']))
{

?><u><h2>Your Playlist</h2></u>
<table>
<tr><th>TrackName</th><th>Composer</th><th>Genre</th><th></th></tr><?php

	for ($i=1; $i<=$count; $i++)
	{
	
	//query for and then print ALL array positions starting at 1(skip 0), EACH time the page loads/'add' is clicked
	//only the last one appears to print.. but they are all actually reprinting each time page loads/this loop runs
	

		if (isset($_SESSION['playlistarray'][$i]))//if the element exists
		{
		$line=$i;//dont increment line if array element is empty aka element contents were unset/deleted
		$chosenid = ($_SESSION['playlistarray'][$i]);//chosentrackid = trackid from the array element $i
		$query = "Select Name, Composer, GenreId from Track where TrackId = $chosenid"; 
		$result = mysqli_query($conn,$query); if (!$result) {die(mysqli_error($conn));}
			
			if (mysqli_num_rows($result)> 0) 
			{
			$row = mysqli_fetch_assoc($result);

			echo "<tr><td>".$row['Name']."</td>";
			echo "<td>".$row['Composer']."</td>";
			echo "<td>".$row['GenreId']."</td>";

			?><td><form action="comp4assmtBindex.php" method="get">
			<input type='hidden' name='trackidtodelete' size="1" value="<?php echo $_SESSION['playlistarray'][$i]; ?>">
			<input type='hidden' name='lineidtodelete' size="1" value="<?php echo $line; ?>">
			<input type="submit" name="delete" value="remove"></form></td></tr><?php
			}//end if sqli result
		
		}//end if isset array
	
	}//end forloop

}//end if isset playlist count

echo "</table>";
?>




<?php
//////////////////////////////////////////////////PROCESS COOKIES
//if name cookie set, say welcome
if(isset($_COOKIE['name'])){echo "Welcome, " . $_COOKIE['name'] . "!";}

//if discount cookie set, say discount
if(isset($_COOKIE['coupon'])){echo " Order today to receive 25% off your entire order!";}

//if genre1 cookie set, fill $genre1 variable 
//+ check for genre2 and genre3 cookies and fill vars if needed
//+ create query if genre cookies found
if(isset($_COOKIE['genre1']))
{
	$genre1=$_COOKIE['genre1'];
	$query = "select * from Track where GenreId = $genre1 Limit 20";

	if(isset($_COOKIE['genre2']))
		{$genre2=$_COOKIE['genre2'];
		$query = "select * from Track where GenreId = $genre2 or GenreId = $genre1 Limit 20";}

	if(isset($_COOKIE['genre3']))
		{$genre3=$_COOKIE['genre3'];
		$query = "select * from Track where GenreId = $genre1 or GenreId = $genre3 or GenreId = $genre2 Limit 20";}
}
else{
//if no genre cookie exists....
//NOTE to be here at all, you would have a NAME and at least ONE GENRE selected (unless direct via url)
$query = "select * from Track Limit 20";
};
?>

<u><h2>Tracks You May Like</h2></u>
<table>
<tr><th>Name</th><th>GenreID</th><th>Composer</th><th>UnitPrice</th><th></th></tr>

<?php

$result = mysqli_query($conn,$query);
// check for errors
if (!$result) {
die(mysqli_error($conn));
}
// check for results
if (mysqli_num_rows($result)> 0) {
// loop through results and display
while ($row = mysqli_fetch_assoc($result)) {
echo "<tr><td>".$row['Name']."</td>";
echo "<td>".$row['GenreId']."</td>";
echo "<td>".$row['Composer']."</td>";
echo "<td>".$row['UnitPrice']."</td>";

?>

<td>
<!---should be POST not 'gets' or even 'get'--->
<!--needs to be POST because POST['remember'] is not set.....-->
<form method='get' action='comp4assmtBindex.php'>
<!--should call script or cart????-->
<input type='hidden' name='hiddenformid' value="<?php echo $row['TrackId']; ?>">
<input type='submit' name='add' value='Add to playlist'>
</form></td></tr>

<?php
}
} 
?>

</table>
<p><a href="comp4assmtBregister.php">Registration Page</a>


<?php
writeFoot("4B");
?>