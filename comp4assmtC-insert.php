<?php
require_once 'comp4functions.php';
$conn =  createConn();
writeHead("INSERT", "Comp 4.4- User Authentication");


///--PRINT CONFIRMATION OF PRVS ACTION if applicable
if(isset($_GET['actionheadervarinserted'])){
echo "<p>Track ID# " . $_GET['idheadervarinserted'] . " " . $_GET['actionheadervarinserted'] ."....</p>";
}


//if CONFIRM BUTTON below was clicked (which will rerun this page)
if(isset($_POST['submitinsert'])){
$valid=true;


///FORM LINE 0 TRACK ID
//////////////Invoke the databases's default auto-increment value for the trackid w/ word DEFAULT in query
////no html form input needed
////no php validation needed
///no field created on form
/// the default value is inserted into datbase via query on line 130


//////////TEXT BOX///FORM LINE 1////////////////////// NAME aka Album Name //////////
/////// REQUIRED /////////////////// TEXT ///////// MAX 200 CHARS //////
$name = mysqli_real_escape_string($conn, trim($_POST['name']));
if (empty($name)) 
{
echo "<p class='error'>Please enter a name</p>";
$valid = false;
}
if (strlen($name)>200)
{
echo "<p class='error'>(Name allows 200 characters max)</p>";
$valid=false;
}



//////DROP DOWN SELECTION LIST///FORM LINE 2////////// ALBUM ID #////////
///////// REQUIRED ///////////////// DROP DOWN SELECTION /////////////////
$albumid = mysqli_real_escape_string($conn, trim($_POST['albumid']));
if (empty($albumid)) 
{
echo "<p class='error'>Please enter an album id</p>";
$valid = false;
}





//////////TEXT BOX///FORM LINE 3//////////////////// MEDIA TYPE ID /////
//////////PRESET ////////////////// TEXT /////
///SET to 2
$mediatypeid = mysqli_real_escape_string($conn, trim($_POST['mediatypeid']));
if (empty($mediatypeid)) 
{
echo "<p class='error'>Please enter a media type id</p>";
$valid = false;
}
//if(is_numeric($mediatypeid))
if (!ctype_digit($mediatypeid)) 
{
echo "<p class='error'>Media Type ID must be integer</p>";
$valid = false;
}


//////////TEXT BOX///FORM LINE 4//////////////////// GENRE ID ////////
//////////PRESET ////////////////// TEXT /////
///SET to 1
$genreid = mysqli_real_escape_string($conn, trim($_POST['genreid']));
if (!ctype_digit($genreid)) {
echo "<p class='error'>Genre ID must be integer</p>";
$valid = false;
}




//////////TEXT BOX///FORM LINE 5/////////////////// COMPOSER ////////
//////////OPTIONAL ////////////////// TEXT ///// MAX 220 CHARS /////
$composer= mysqli_real_escape_string($conn, trim($_POST['composer']));
if (strlen($name)>220)
{
echo "<p class='error'>(Name allows 220 characters max)</p>";
$valid=false;
}


//////////TEXT BOX///FORM LINE 6/////////////////// MILLISECONDS /////
//////////REQUIRED ////////////////// INTEGER ///////
$milliseconds = mysqli_real_escape_string($conn, trim($_POST['milliseconds']));
if (empty($milliseconds)) 
{
echo "<p class='error'>Please enter milliseconds</p>";
$valid = false;
}
if (!ctype_digit($milliseconds)) 
{
echo "<p class='error'>Milliseconds must be integer</p>";
$valid = false;
}



///////////TEXT BOX///FORM LINE 7/////////////////// BYTES/////////
////////// OPTIONAL ///////////////// INTEGER //////////
$bytes = mysqli_real_escape_string($conn, trim($_POST['bytes']));
if (!ctype_digit($bytes)) 
{
echo "<p class='error'>Bytes must be integer</p>";
$valid = false;
}


//////////TEXT BOX///FORM LINE 8////////////////// UNIT PRICE///////
//////////REQUIRED///////////// DECIMAL WITH 2 PLACES, MAX 99,999,999,99///////////////
$unitprice= mysqli_real_escape_string($conn, trim($_POST['unitprice']));
if(empty($unitprice) or !is_numeric($unitprice) or (!preg_match('/^[0-9]{1,10}(.[1-9]{2})?$/',$unitprice)))
{
echo"<p class='error'>numeric price as decimal required, 1-10 digits left of decimal and 2 right of decimal</p>";
$valid=false;
}


/////////////////////////////////////DONE GATHERING DATA FROM FORM////////////////////////////////////////

if ($valid) {
$query = "insert into Track values(default, '$name','$albumid','$mediatypeid','$genreid','$composer','$milliseconds','$bytes','$unitprice')";
mysqli_query($conn, $query) or die(mysqli_error($conn));
//if database changed
if (mysqli_affected_rows($conn)>0) {
    $tid = mysqli_insert_id($conn);
    header("Location: comp4assmtC-display.php?actionheadervarinserted=added&idheadervarinserted=$tid");
//send back to this same page, with header info to trigger message confirming what we have done
    exit();
}
//if datbase did NOT change
echo "<p class='error'>Unable to insert record</p>";
}

}

else {
// if the form was not submitted, initialize the variables for the sticky form fields


$name="";
$albumid="";
$mediatypeid="";
$genreid="";
$composer="";
$milliseconds="";
$bytes="";
$unitprice="";

}
?>









<form method="post" action="comp4partC-insert.php">



<!---///FORM LINE 0 TRACK ID
//////////////Accept the default auto-increment value for the trackid 
////no html form input needed
////no php validation needed
///no field created on form
/// the default value is inserted into database via query on line 133--->




<!--//////////TEXT BOX///FORM LINE 1////////////////////// NAME aka Track Name///////////
////// REQUIRED ///////////////// TEXT ///////// MAX 200 CHARS //////-->	
<p>
<label for="name">Track Name:</label>
<input type="text" name="name" id="name"  value="<?php echo $name; ?>">
</p>


<!--//////DROP DOWN SELECTION LIST///FORM LINE 2////////// ALBUM ID#///////
////////// REQUIRED ///////////////// SELECTION ////////
Populates from database table /////////-->	
<p>
<label for="albumid">Album Name:</label>
<select name="albumid" id="albumid">
<?php
$query = "Select AlbumId, Title from Album";//query ALBUM TABLE
$result = mysqli_query($conn,$query);
if (!$result){
die(mysqli_error($conn));
}
if (mysqli_num_rows($result)> 0)
{ //if anything pulled from query..........
while ($row = mysqli_fetch_assoc($result)) 
{ //while something is there, aka fetch and print them ALL as dropdown items
echo "<option value='".$row['AlbumId']."'";

// if the album id matches any album id/row in the list.. 
if ($albumid==$row['AlbumId']) {echo " selected ";//flag as preselected option on the dropdown box
//NOTE the album id field is BLANK at this point because we started at line 148 and albumid field was intialized to blank
//as a result, this IF statment will not trigger... 'selected' will not be instered into the option.. 
//and the option will default to the top item in the list of albums since 'selected' is not specified
}
echo ">".$row['Title']."</option>";//show album title only, do not display album id
}
}
?>
</select>
</p>




<!--//////////TEXT BOX///FORM LINE 3//////////////////// MEDIA TYPE ID ////////// OPTIONAL /////////
//////// TEXT /////
///SET to 2-->
<label for="mediatypeid">Media Type ID:</label>
<input type="text" name="mediatypeid" id="mediatypeid" value="2" size="2">
</p>

<!--//////////TEXT BOX///FORM LINE 4//////////////////// GENRE ID /////////////// OPTIONAL /////////
//////// TEXT /////	
///SET to 1//////-->
<p>
<label for="genreid">Genre ID:</label>
<input type="text" name="genreid" id="genreid" value="1" size="2">
</p>




<!--//////////TEXT BOX///FORM LINE 5/////////////////// COMPOSER //////////////// OPTIONAL ////////
///////// TEXT ///// MAX 220 CHARS /////-->	
<p>
<label for="composer">Composer:</label>
<input type="text" name="composer" id="composer" value="<?php echo $composer; ?>">
</p>

<!--//////////TEXT BOX///FORM LINE 6/////////////////// MILLISECONDS /////////// REQUIRED ///////
////////// INTEGER ///////-->	
<p>
<label for="milliseconds">Milliseconds:</label>
<input type="text" name="milliseconds" id="milliseconds" value="<?php echo $milliseconds; ?>">
</p>

<!--//////////TEXT BOX///FORM LINE 7/////////////////// BYTES/////////////////// OPTIONAL ////////
///////// INTEGER //////////-->		
<p>
<label for="bytes">Bytes:</label>
<input type="text" name="bytes" id="bytes" value="<?php echo $bytes; ?>">
</p>

<!--//////////TEXT BOX///FORM LINE 8////////////////// UNIT PRICE///////////////REQUIRED//////
/////// DECIMAL WITH 2 PLACES, MAX 99,999,999,99///////////////-->	
<p>
<label for="unitprice">Unit Price:</label>
<input type="text" name="unitprice" id="unitprice" value="<?php echo $unitprice; ?>">
</p>

<!--//////////BUTTON///FORM LINE 9/////////////////////-->
<p>
<input type="submit" name="submitinsert" value="Insert Track">
</p>

</form>

<p>Cancel &  <a href="comp4assmtC-display.php">Return to Display Page</a></p>

<?php writeFoot("4C"); ?>