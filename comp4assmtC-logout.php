<?php
require_once 'comp4functions.php';


     session_start();

     // unset session variables

     unset($_SESSION);

     // destroy the session

     session_destroy();
     writeHead("LOGOUT", "Lab Comp 4.4- User Authentication");

?>
<h1>You are logged out.</h1>
<h2>Thank you for visiting</h2>
<p><a href="comp4assmtC-list.php">Back to list</a></p>
<?php writeFoot("4C") ?>