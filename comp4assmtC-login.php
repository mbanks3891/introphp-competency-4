<?php
// buffer the output
ob_start();
require_once 'comp4functions.php';
writeHead("LOGIN", "Comp 4.4- User Authentication");

// check to see if a logintries cookie is set. 
if (isset($_COOKIE['logintries'])) 
{
// if the user has tried to login unsuccessfully more than 4 times, send them to an error page
	if ($_COOKIE['logintries']>4) 
	{
	header("location: comp4assmtC-list.php");
	}
}


// set a variable to check credentials only if id and password are found in cookies or in the form
$cred=false;

// check to see if login cookies are present. If so, get cookie values.
if (isset($_COOKIE['uid'])) 
{
$userid = $_COOKIE['uid'];
$pw = $_COOKIE['pw'];
$cred=true;
}


// check to see if the form has been submitted
if (isset($_POST['submit'])) 
{
// get form fields
$userid = $_POST['userid'];
//encrypt password
$pw = md5($_POST['password']);
// set check credentials flag to true
$cred = true;
}


// if login credentials have been entered, check for authorization
if ($cred) 
{
$dbConn= createConn();
$query = "select * from UserMB where userid='$userid' and password='$pw';";
$result = mysqli_query($dbConn,$query);
    
    // check to see if a match was found, if so, get the authorization level
	if ($result && mysqli_num_rows($result)==1) 
	{
	$row = mysqli_fetch_array($result);

	session_start();
	// set a session variable with the authorization level
	$_SESSION['auth']=$row['auth'];
	$_SESSION['userid']=$userid;
	// check to see if a logintries cookie is set. If so, delete it.

		if (isset($_COOKIE['logintries'])) 
		{
		setcookie('logintries',false,time()-1000);
		}

		// check to see if the user wants automatic login
		if (isset($_POST['autologin'])) 
		{
		// save the login info in cookies valid for 14 days
		setcookie('uid',$userid,time()+(60*60*24*14));
		setcookie('pw',$pw,time()+(60*60*24*14));
		}

		// check to see if a page was passed in the query string. If not go to the list page
		if (isset($_POST['page'])) 
		{
		$page = $_POST['page'];
		} 
		else 
		{
		$page = "comp4assmtC-display.php";
		}
	// user authorized so transfer back to page or to a default page
	header("location: $page");

	}//end if match found 


	else 
	{//if match not found

		// see if a logintries cookie exists
	    if (isset($_COOKIE['logintries'])) 
	    {
	    // if found, add 1 to the tries else set it to 1
	    $tries = $_COOKIE['logintries'] + 1;
	    } 
	    else 
	    {
	    $tries = 1;
	    }

    // create a cookie to count the number of unsuccessful login attempts
    setcookie('logintries',$tries,time()+(60*60*24));
        
        // if too many tries, send to error page
        if ($tries > 4) 
        {
        header("location: comp4assmtC-error.php");
        }

    // if no match, the user is not authorized so try again
    $msg = "Invalid login credentials. You have ".(5-$tries)." more attempts";
    }


//writeHead("Desired Lab Comp 4.4", "User Authentication");
}//endif $cred
?>

<form method="post" action="comp4assmtC-login.php">

<?php
if (isset($msg)) 
{
echo "<p class='error'>$msg</p>";
}
?>

<p><label>User ID: 
<input type="text " name="userid"></label>
</p>
    
<p><label>Password: 
<input type="password" name="password"></label>
</p>
    
<p><label>
<input type="checkbox" name="autologin" value="true"> Log me in automatically</label>
</p>

<p><input type="submit" name="submit" value="Login">
</p>

<?php
if (isset($_GET['page'])) 
{
echo "<input type='hidden' name='page' value='".$_GET['page']."'>";
}
?>
</form>

<p>No Account? <a href="comp4assmtC-register.php">Register Now</a></p>

<?php writeFoot("4C"); ?>