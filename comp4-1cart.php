<?php
// check to see if the empty cart form was submitted
if (isset($_POST['empty'])) 
{
// to empty cart set the cart cookie to false and the expiration to a time in the past
setcookie('cartlinesqtycookie',false,time()-1000);
//wipe the other cookies too
setcookie('trackidcookieforline'.$totalitems,false,time()-1000);
setcookie('trackqtycookieforline'.$totalitems,false,time()-1000);
// transfer to the cart page (this page) to trigger the cookie reset (note that you can't change a cookie and immediately see the results until the page is reloaded
header("location: comp4lab4-1cart.php");
exit();
}





require_once 'comp4functions.php';
//require_once 'comp4lab4-1script.php'; ///need to include the script page?
writeHead("Desired Comp4.1", "Cookie Shopping Cart 2");



// check to see if the cart cookie exists 
// if 'cart' cookie does not exist, display the empty cart message
if (!isset($_COOKIE['cartlinesqtycookie'])) 
{
echo "<h3>Your Shopping Cart is empty</h3>";
//test echo to see contents of 'cart' cookie..........
//this heleped because i got an error message 'undefined index' for cookie...  and was able to goole that error
//https://www.daniweb.com/programming/web-development/threads/421107/undefined-index-cookie-problem
echo $_COOKIE['cartlinesqtycookie'];
} 
else 
{//if 'cart' cookie does exist, proceed with table 
?>


<div>
<table>
<tr><th>Track ID</th><th>Name</th><th>Unit Price</th><th>Quantity</th><th>Total</th></tr>

<?php
// connect to the database
$conn = createConn();

// get the cart cookie to know how many items have been added to cart
$itemcount = $_COOKIE['cartlinesqtycookie'];

// initialize a cart total, cartqty and counter before the loop
$cartTotal = 0;
$cartqty = 0;
$ctr = 1;

// loop through cart cookies 
while ($ctr <=$itemcount) {
// get track and quantity for each cookie using the ctr to identify which cookie to get
$track = $_COOKIE['trackidcookieforline'.$ctr];
$qty = $_COOKIE['trackqtycookieforline'.$ctr];
// create the query to get the name and price from the Track table in the database
$query = "Select Name, UnitPrice from Track where TrackId = $track";
// run the query
$result = mysqli_query($conn,$query);
// check for errors
if (!$result) {
die(mysqli_error($conn));
}

// get results and display
$row = mysqli_fetch_assoc($result);
echo "<tr><td>$track</td>";
echo "<td>".$row['Name']."</td>";
echo "<td>".$row['UnitPrice']."</td>";
echo "<td>$qty</td>";

// calculate the total by multiplying the price times the quantity
$total = $qty * $row['UnitPrice'];

// add the total and quantity to the cart totals
$cartTotal += $total;
$cartqty += $qty;

// display the line total in the table
echo "<td align='right'>$total</td></tr>";

// increment the counter
$ctr++;
} // end while

//write out the totals for the cart
echo "<tr><td></td><td><b>Cart Total</b></td>";
echo "<td></td><td><b>$cartqty</b></td><td align='right'><b>$ $cartTotal</b></td></tr>";
} // end if
?>

</table>
<p><form action="comp4lab4-1cart.php" method="post"><input type="submit" value="Empty Cart" name="empty"></form></p>
<p><a href="comp4-1script.php">Go to Order Form</a></p>
</div>
<?php 
writeFoot(4.1);
?>