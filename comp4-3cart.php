

<?php
session_start();


if (isset($_POST['empty'])) {
// to empty cart unset session variables and destroy session
unset($_SESSION);
session_destroy();
}


require_once 'comp4functions.php';
writeHead("Lab 4-3", "Cookie Shopping Cart");


//if cart session does not exist, display the empty cart message
if (!isset($_SESSION['cart'])) {
echo "<h3>Your Shopping Cart is empty</h3>";
} 



else {
//if cart session variable exist, print cart data
?>
<div>
<table>
<tr><th>Track ID</th><th>Name</th><th>Unit Price</th><th>Quantity</th><th>Total</th></tr>
<?php
$conn =  createConn();
// get the cart cookie to know how many items have been added to cart
$itemcount = $_SESSION['cart'];
// initialize a cart total and cartqty  before the loop
$cartTotal = 0;
$cartqty = 0;
// loop through session variable arrays 
for ($i=1; $i <=$itemcount; $i++) {
// get track and quantity for each element using the counter to identify which cookie to get
$track = $_SESSION['item'][$i];
$qty = $_SESSION['qty'][$i];
// create the query to get the name and price from the Track table in the database

$query = "Select Name, UnitPrice from Track where TrackId = $track";
$result = mysqli_query($conn,$query);
if (!$result) {die(mysqli_error($conn));}
$row = mysqli_fetch_assoc($result);
echo "<tr><td>$track</td>";
echo "<td>".$row['Name']."</td>";
echo "<td>".$row['UnitPrice']."</td>";
echo "<td>$qty</td>";

// calculate the total by multiplying the price times the quantity
$total = $qty * $row['UnitPrice'];
    
// add the total and quantity to the cart totals
$cartTotal += $total;
$cartqty += $qty;
// display the line total in the table
echo "<td align='right'>$total</td></tr>";
} // end for loop
//write out the totals for the cart
echo "<tr><td></td><td><b>Cart Total</b></td>";
echo "<td></td><td><b>$cartqty</b></td><td align='right'><b>$ $cartTotal</b></td></tr>";
} // end if
?>
</table>
<p><form action="comp4lab4-3cart.php" method="post"><input type="submit" value="Empty Cart" name="empty"></form></p>
<p><a href="comp4-3script.php">Go to Order Form</a></p>
</div>
<?php 
writeFoot(4.2);
?>