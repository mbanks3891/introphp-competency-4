
<?php

ob_start();


require_once "comp4functions.php";
writeHead("INDEX","Competency 4, Part A- Read & Write Cookies");
$conn =  createConn();


session_start();


if (isset($_GET['add']))
{
$tracknum=$_GET['hiddenformid'];

	if (isset($_SESSION['playlistcount']))
	{
	$_SESSION['playlistcount']++;
	$count = $_SESSION['playlistcount'];
	//$_SESSION['playlisttrackarray'][$items]=$track;// array position=track name
	}
	else{
	$_SESSION['playlistcount']=1;
	$count=1;
	}
$_SESSION['playlistarray'][$count]=$tracknum; //latest spot in array gets trackid you're adding
}





if (isset($_SESSION['playlistcount']))
{

$count=$_SESSION['playlistcount'];

?><u><h2>Your Playlist</h2></u>
<table>
<tr><th>TrackName</th><th>Composer</th><th>Genre</th><th></th></tr><?php

for ($i=1; $i<=$count; $i++){
//print array positions starting at 1, each time the page loads/'add' is clicked
//note array position 0 is not used....

//get the value of 'playlisttracksarray' session var
$chosenid = ($_SESSION['playlistarray'][$i]);//chosentrackid = trackid from the array position $i
$query = "Select Name, Composer, GenreId from Track where TrackId = $chosenid"; 
$result = mysqli_query($conn,$query);
if (!$result) {die(mysqli_error($conn));}
if (mysqli_num_rows($result)> 0) {
$row = mysqli_fetch_assoc($result);

echo "<tr><td>".$row['Name']."</td>";
echo "<td>".$row['Composer']."</td>";
echo "<td>".$row['GenreId']."</td>";

//$_SESSION['playlistcount']=$items;//omit... preventing delete from working 5-3-19-F-1853

?><td><form action="comp4assmtBindex.php" method="get">
<input type='hidden' name='idfordelete' value="<?php echo $chosenid; ?>">
<input type="submit" name="delete" value="remove"></form></td></tr><?php

}//end if sqli result
}//end for loop, end table output loop
}//end if isset playlist count
//}//end if isset add

?></table>






<?php
if (isset($_GET['delete']))
{
$idtodelete=$_GET['idfordelete'];
$_SESSION['playlistcount']= $_SESSION['playlistcount']-1;
$count=$_SESSION['playlistcount'];

	for ($i=1; $i<=$count; $i++)
	{
	$idselected = ($_SESSION['playlistarray'][$i]);
		if ($idselected == $idtodelete)
		{
		unset($_SESSION['playlistarray'][$i]);
		}

	}//end for 


if ($_SESSION['playlistcount'] == 0)
{
unset($_SESSION);
session_destroy();
}	
	
}// end if isset delete





//end if isset POST[add] 
//pull the track name from form
//should update each time a new item is clicked.........

///create /update session variables
/*
if (isset($_SESSION['playlistcount'])) 
{
$items = ($_SESSION['playlistcount'])+1;
$_SESSION['playlisttrackarray'][$items]=$track;// array position=track name
} 
else {
$_SESSION['playlistcount']=1;
$items=1;
$_SESSION['playlisttrackarray'][$items]=$track;//array position= track name
}
*/










//}//end if isset DELETE


?>





<?php
//////////////////////////////////////////////////PROCESS COOKIES
//if name cookie set, say welcome
if(isset($_COOKIE['name'])){echo "Welcome, " . $_COOKIE['name'] . "!";}

//if discount cookie set, say discount
if(isset($_COOKIE['coupon'])){echo " Order today to receive 25% off your entire order!";}

//if genre1 cookie set, fill $genre1 variable 
//+ check for genre2 and genre3 cookies and fill vars if needed
//+ create query if genre cookies found
if(isset($_COOKIE['genre1']))
{
	$genre1=$_COOKIE['genre1'];
	$query = "select * from Track where GenreId = $genre1 Limit 20";

	if(isset($_COOKIE['genre2']))
		{$genre2=$_COOKIE['genre2'];
		$query = "select * from Track where GenreId = $genre2 or GenreId = $genre1 Limit 20";}

	if(isset($_COOKIE['genre3']))
		{$genre3=$_COOKIE['genre3'];
		$query = "select * from Track where GenreId = $genre1 or GenreId = $genre3 or GenreId = $genre2 Limit 20";}
}
else{
//if no genre cookie exists....
//NOTE to be here at all, you would have a NAME and at least ONE GENRE selected (unless direct via url)
$query = "select * from Track Limit 20";
};
?>

<u><h2>Tracks You May Like</h2></u>
<table>
<tr><th>Name</th><th>GenreID</th><th>Composer</th><th>UnitPrice</th><th></th></tr>

<?php

$result = mysqli_query($conn,$query);
// check for errors
if (!$result) {
die(mysqli_error($conn));
}
// check for results
if (mysqli_num_rows($result)> 0) {
// loop through results and display
while ($row = mysqli_fetch_assoc($result)) {
echo "<tr><td>".$row['Name']."</td>";
echo "<td>".$row['GenreId']."</td>";
echo "<td>".$row['Composer']."</td>";
echo "<td>".$row['UnitPrice']."</td>";

?>

<td>
<!---should be POST not 'gets' or even 'get'--->
<!--needs to be POST because POST['remember'] is not set.....-->
<form method='get' action='comp4assmtBindex.php'>
<!--should call script or cart????-->
<input type='hidden' name='hiddenformid' value="<?php echo $row['TrackId']; ?>">
<input type='submit' name='add' value='Add to playlist'>
</form></td></tr>

<?php
}
} 
?>

</table>
<p><a href="comp4assmtAregister.php">Registration Page</a>


<?php
writeFoot("4B");
?>