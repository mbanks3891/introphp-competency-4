<?php
// buffer the output
ob_start();
require_once 'comp4functions.php';


if (isset($_POST['submit'])) {
// start the session
session_start();



// Get the values from the form. 
//If a valid quantity was not entered, default to 1
if (!is_numeric($_POST['qty'])) {
$qty = 1;
} 
else {
$qty = $_POST['qty'];
} 


$track = $_POST['track'];

    
    


// check to see if the cart session variable exists
if (isset($_SESSION['cart'])) {
// if cart variable is found, add 1 to the cart value to get the number of items. Otherwise set items to
$items = $_SESSION['cart']+1;
} 
else {
$items = 1;
}


// create or update 3 session variables: cart, item[x] and qty[x] where x is the value of the $items variable. 
$_SESSION['cart']=$items;
$_SESSION['item'][$items]=$track;
$_SESSION['qty'][$items]=$qty;
} //end if for form submission



writeHead("Lab 4-3", "Session Shopping Cart");
?>
<div>
<table>
<tr><th>Track ID</th><th>Name</th><th>Unit Price</th><th> </th></tr>
<?php
// connect to the database
$conn =  createConn();
// create the query
$query = "Select TrackId, Name, UnitPrice from Track limit 20";
// run the query
$result = mysqli_query($conn,$query);
// check for errors
if (!$result) {
die(mysqli_error($conn));
}
// check for results
if (mysqli_num_rows($result)> 0) {
// loop through results and display
while ($row = mysqli_fetch_assoc($result)) {
echo "<tr><td>".$row['TrackId']."</td>";
echo "<td>".$row['Name']."</td>";
echo "<td>".$row['UnitPrice']."</td>";
// add form for add to cart with a quantity input textbox, the trackid in a hidden field and a submit button
?>
<td>
<form method='post' action='comp4lab4-3script.php'>
<label>Qty: <input type='text' name='qty' size='2'></label>
<input type='hidden' name='track' value="<?php echo $row['TrackId']; ?>">
<input type='submit' name='submit' value='Add to cart'>
</form></td></tr>
<?php
} // end while loop
} // end if
?>
</table>



<p><a href="comp4-3cart.php">View Shopping Cart</a></p>
</div>

<?php writeFoot(4.2); ?>